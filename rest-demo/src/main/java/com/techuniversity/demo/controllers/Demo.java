package com.techuniversity.demo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(path="/saludos")

public class Demo {

    @GetMapping("/saludo")
    public String saludo(@RequestParam(name = "nombre") String n){
        return "Hola " + n;
    }

    @GetMapping("/saludo1")
    public String saludo1(@RequestParam(name = "nombre", required = false) String n){
        if (n == null) n = "desconocido";
        return "Hola " + n;
    }

    @GetMapping("/saludo2")
    public String saludo2(@RequestParam(name = "nombre") Optional<String> n){
        return "Hola " + n.orElse("desconocido");
    }

    @GetMapping("/saludo3")
    public String saludo3(@RequestParam(name = "nombre", defaultValue = "desconocido") Optional<String> n){
        return "Hola " + n;
    }

    @GetMapping("/multiSaludo")
    public String multiSaludo(@RequestParam() Map<String, String> todos){
        return "Los parámetros " + todos.entrySet();
    }

    @GetMapping("/saludos")
    public String saludos(@RequestParam() List<String> nombres){
        String saludo = "";
        for (String nombre : nombres){
            saludo += "Hola " + nombre + "\n";
        }
        return saludo;
    }

}
