package com.techuniversity.prod.servicios;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ServicioService {
    MongoCollection<Document> servicios;

    private static MongoCollection<Document> getServiciosCollection() {
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient cliente = MongoClients.create(settings);
        MongoDatabase database = cliente.getDatabase("backMayo");
        return database.getCollection("servicios");
    }

    public static List getAll(){
        MongoCollection<Document> servs = getServiciosCollection();
        List lista = new ArrayList();
        FindIterable<Document> iterDoc = servs.find();
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            lista.add(it.next());
        }

        return lista;
    }

    public static void insert(String strServ) throws Exception {
        Document doc = Document.parse(strServ);
        MongoCollection<Document> servs = getServiciosCollection();
        servs.insertOne(doc);
    }

    public static void insertBatch(String strServ) throws Exception {
        Document doc = Document.parse(strServ);
        List<Document> listServ = doc.getList("servicios", Document.class);
        MongoCollection<Document> servs = getServiciosCollection();
        servs.insertMany(listServ);
    }

    public static List<Document> getFiltrados(String strFiltro) {
        MongoCollection<Document> servs = getServiciosCollection();
        List lista = new ArrayList();
        Document docFiltro = Document.parse(strFiltro);
        FindIterable<Document> iterDoc = servs.find(docFiltro);
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            lista.add(it.next());
        }
        return lista;
    }
    public static List<Document> getFiltradosPeriodo(Document docFiltro) {
        MongoCollection<Document> servs = getServiciosCollection();
        List lista = new ArrayList();
        FindIterable<Document> iterDoc = servs.find(docFiltro);
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            lista.add(it.next());
        }
        return lista;
    }

    public static void update(String filtro, String valores) {
        MongoCollection<Document> servs = getServiciosCollection();
        Document docFiltro = Document.parse(filtro);
        Document docValores = Document.parse(valores);
        servs.updateOne(docFiltro, docValores);
    }
}
