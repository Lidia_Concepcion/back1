package com.techuniversity.prod.controllers;

import com.techuniversity.prod.servicios.ServicioService;
import org.bson.Document;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/servicios")
public class ServicioController {

    /*@GetMapping("/servicios")
    public List getServicios() {
        return ServicioService.getAll();
    }*/

    @PostMapping("/servicios")
    public String setServicios(@RequestBody String str) {
        try {
            ServicioService.insertBatch(str);
            return "OK";
        } catch (Exception e) {
            return e.getMessage();
        }
    }
    @PostMapping("/servicio")
    public String setServicio(@RequestBody String str) {
        try {
            ServicioService.insert(str);
            return "OK";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @GetMapping("/servicios")
    public List getServicios(@RequestBody String filtro) {
        return ServicioService.getFiltrados(filtro);
    }

    @GetMapping("/servicios/periodo")
    public List getServiciosPeriodos(@RequestParam String periodo) {
        Document doc = new Document();
        doc.append("disponibilidad.periodos", periodo);
        return ServicioService.getFiltradosPeriodo(doc);
    }

    @PutMapping ("/servicios")
    public String updServicios(@RequestBody String data) {
        try {
            JSONObject obj = new JSONObject(data);
            String filtro = obj.getJSONObject("filtro").toString();
            String valores = obj.getJSONObject("valores").toString();
            ServicioService.update(filtro,valores);

            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }
}
