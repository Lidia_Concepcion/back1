package com.techuniversity.prod;

import com.techuniversity.prod.productos.ProductoModel;
import com.techuniversity.prod.productos.ProductoRepository;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class ProductoRepoIntegrationTest {

    @Autowired
    ProductoRepository productoRepository;

    @Test
    public void testFindAll(){
        List<ProductoModel> productos = productoRepository.findAll();
        assertTrue(productos.size() > 0);
    }

    @LocalServerPort
    private int port;
    TestRestTemplate testRestTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Test
    public void testPrimerProducto() throws Exception {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = testRestTemplate.exchange(
                crearUrlPort("/productos/productos/60aca8609af7fb4379b257ef"),
                HttpMethod.GET, entity, String.class
        );
        String expected = "{\"id\":\"60aca8609af7fb4379b257ef\"," + "\"nombre\":\"Producto 1\",\"descripcion\":\"Desc. ampliada Producto 1\"," + "\"precio\": 10.5}";
        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

    private String crearUrlPort(String url) {
        return "http://localhost:" + port + url;
    }

}
