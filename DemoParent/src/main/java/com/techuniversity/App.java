package com.techuniversity;

import jdk.nashorn.internal.runtime.JSONFunctions;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        App app = new App();
        Properties props = app.loadPropertiesFile("db.properties");
        props.forEach((k,v) -> System.out.println(k + ": " + v));

    }

    public Properties loadPropertiesFile(String filename){
        Properties props = new Properties();

        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream(filename);
            props.load(is);
        } catch (IOException ex){
            System.out.println("Imposible acceder a los recursos.");
        }

        return props;
    }
}
