package com.techuniversity.emp;

import com.techuniversity.emp.controllers.EmpleadosController;
import com.techuniversity.emp.utils.BadSeparator;
import com.techuniversity.emp.utils.EstadosPedido;
import com.techuniversity.emp.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class EmpleadosControllerTest {

    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testCadena(){
        String esperado = "L.U.Z S.O.L";
        assertEquals(esperado, empleadosController.getCadena("luz sol", "."));
    }

    @Test
    public void testBadSeparator(){
        try {
            Utilidades.getCadena("Lidia Con", "  .");
            fail("Se esperaba Bad Separator");
        } catch (BadSeparator bs) {}
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 5, -3, 21, Integer.MAX_VALUE})
    public void testEsImpar(int num) {
        assertTrue(Utilidades.esImpar(num));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "    "})
    public void testEnBlanco(String str) {
        assertTrue(Utilidades.enBlanco(str));
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    public void testEnBlancoNull(String str) {
        assertTrue(Utilidades.enBlanco(str));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", "    ", "\t", "\n"})
    public void testEnBlancoNullEmpty(String str) {
        assertTrue(Utilidades.enBlanco(str));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorEstadoPedido(EstadosPedido estado) {
        assertTrue(Utilidades.valorEstadoPedido(estado));
    }
}
