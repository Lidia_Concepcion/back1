package com.techuniversity.emp.utils;

import java.util.Locale;

public class Utilidades {

    public static String getCadena (String texto, String separador) throws BadSeparator {
        if (separador.length() > 1 || separador.trim().equals(" ")) throw new BadSeparator();

        char[] letras = texto.toUpperCase().toCharArray();
        String resultado = "";
        boolean espacio = true;

        for (char l:letras) {
            if (l != ' ') {
                if (!espacio) resultado += separador;
                espacio = false;
            } else {
                espacio = true;
            }
            resultado += l;
        }
        return resultado;
    }

    public static String getCadenaOri (String texto, String separador) {
        char[] letras = texto.toUpperCase().toCharArray();
        String resultado = "";

        for (char l:letras) {
            if (l != ' ')
                resultado += l + separador;
            else {
                if (!resultado.trim().equals("")){
                    resultado = resultado.substring(0, resultado.length() - 1);
                }
                resultado += l;
            }
        }
        if (resultado.endsWith(separador))
            resultado = resultado.substring(0, resultado.length() - 1);

        return resultado;
    }

    public static boolean esImpar (int num) {
        return (num % 2 != 0);
    }

    public static boolean enBlanco(String texto) {
        return ((texto == null) || (texto.trim().isEmpty()));
    }

    public static boolean valorEstadoPedido (EstadosPedido estado) {
        int valor = estado.ordinal();
        return (valor >= 0 && valor <= 4);
    }
}
