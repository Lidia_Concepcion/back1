package com.techuniversity.emp.repositorios;

import com.techuniversity.emp.model.Empleado;
import com.techuniversity.emp.model.Empleados;
import com.techuniversity.emp.model.Formacion;
import org.springframework.stereotype.Repository;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Repository
public class EmpleadosDAO {

    private static Empleados lista = new Empleados();
    static{
        Formacion form1 = new Formacion("2021/01/01", "Curso Back");
        Formacion form2 = new Formacion("2020/01/01", "Curso Front");
        Formacion form3 = new Formacion("2019/01/01", "Curso SQL");

        ArrayList<Formacion> unaForm = new ArrayList<Formacion>();
        unaForm.add(form1);
        ArrayList<Formacion> dosForm = new ArrayList<Formacion>();
        dosForm.add(form1);
        dosForm.add(form2);
        ArrayList<Formacion> allForm = new ArrayList<Formacion>();
        allForm.add(form3);
        allForm.addAll(dosForm);

        lista.getListaEmpleados().add(new Empleado(1, "Pepe", "Perez", "pp@gmail.com", unaForm));
        lista.getListaEmpleados().add(new Empleado(2, "Marta", "Martinez", "mm@gmail.com", dosForm));
        lista.getListaEmpleados().add(new Empleado(3, "Fernando", "Fernandez", "ff@gmail.com", allForm));

    }

    public Empleados getAllEmpleados() {
        return lista;
    }

    public Empleado getEmpleado(int id) {
        for (Empleado emp : lista.getListaEmpleados()) {
            if (emp.getId() == id) {
                return emp;
            }
        }
        return null;
    }

    public void addEmpleado(Empleado emp){
        lista.getListaEmpleados().add(emp);
    }

    public Empleado updateEmpleado(Empleado emp) {
        Empleado currentEmp = getEmpleado(emp.getId());
        if (currentEmp != null){
            currentEmp.setNombre(emp.getNombre());
            currentEmp.setApellidos(emp.getApellidos());
            currentEmp.setEmail(emp.getEmail());
        }
        return currentEmp;
    }

    public Empleado updateEmpleado(int id, Empleado emp) {
        Empleado currentEmp = getEmpleado(id);
        if (currentEmp != null){
            currentEmp.setNombre(emp.getNombre());
            currentEmp.setApellidos(emp.getApellidos());
            currentEmp.setEmail(emp.getEmail());
        }
        return currentEmp;
    }

    public void deleteEmpleado(int id) {
        Iterator itr = lista.getListaEmpleados().iterator();
        while (itr.hasNext()){
            Empleado emp = (Empleado) itr.next();
            if (emp.getId() == id) {
                itr.remove();
                break;
            }
        }
    }

    public Empleado softUpdateEmpleado(int id, Map<String, Object> updates) {
        Empleado current = getEmpleado(id);
        if (current != null){
            for(Map.Entry<String, Object> upd : updates.entrySet()){
                switch (upd.getKey()){
                    case "nombre":
                        current.setNombre(upd.getValue().toString());
                        break;
                    case "apellidos":
                        current.setApellidos(upd.getValue().toString());
                        break;
                    case "email":
                        current.setEmail(upd.getValue().toString());
                        break;
                }
            }
        }
        return current;
    }

    public List<Formacion> getFormaEmpleado(int id){
        for (Empleado emp : lista.getListaEmpleados()){
            if (emp.getId() == id){
                return emp.getFormaciones();
            }
        }
        return null;
    }

    public Empleado addFormaEmpleado (int id, Formacion forma) {
        Empleado current = getEmpleado(id);
        if (current != null) {
            current.getFormaciones().add(forma);
        }
        return current;
    }
}
