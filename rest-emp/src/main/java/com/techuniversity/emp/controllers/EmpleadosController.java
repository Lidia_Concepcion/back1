package com.techuniversity.emp.controllers;

import com.techuniversity.emp.model.Empleado;
import com.techuniversity.emp.model.Empleados;
import com.techuniversity.emp.model.Formacion;
import com.techuniversity.emp.repositorios.EmpleadosDAO;
import com.techuniversity.emp.utils.Configuracion;
import com.techuniversity.emp.utils.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.text.Normalizer;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosController {

    @Autowired
    private EmpleadosDAO empleadosDAO;

    @GetMapping(path = "/", produces = "application/json")
    public Empleados getEmpleados(){
        return empleadosDAO.getAllEmpleados();
    }

    /*
    @GetMapping(path ="/{id}")
    public Empleado getEmpleado(@PathVariable int id){
        Empleado emp = empleadosDAO.getEmpleado(id);
        if (emp != null) return emp;
        else return new Empleado();
    } */

    @GetMapping(path = "/{id}")
    public ResponseEntity<Empleado> getEmleado(@PathVariable int id){
        Empleado emp = empleadosDAO.getEmpleado(id);
        if (emp != null) return new ResponseEntity<>(emp, HttpStatus.OK);

        return ResponseEntity.notFound().build();
    }

    @PostMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp) {
        Integer id = empleadosDAO.getAllEmpleados().getListaEmpleados().size() +1;
        emp.setId(id);
        empleadosDAO.addEmpleado(emp);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(emp.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updateEmpleado(@RequestBody Empleado emp) {
        Empleado modEmp = empleadosDAO.updateEmpleado(emp);

        if (modEmp != null) return ResponseEntity.ok().build();

        return ResponseEntity.notFound().build();
    }

    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updateEmpleado(@PathVariable int id, @RequestBody Empleado emp) {
        Empleado modEmp = empleadosDAO.updateEmpleado(id, emp);

        if (modEmp != null) return ResponseEntity.ok().build();

        return ResponseEntity.notFound().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> deleteEmpleado(@PathVariable int id) {
        empleadosDAO.deleteEmpleado(id);
        return ResponseEntity.ok().build();
    }

    @PatchMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> patchEmpleado(@PathVariable int id, @RequestBody Map<String, Object> updates) {
        Empleado emp = empleadosDAO.softUpdateEmpleado(id, updates);
        if (emp != null) return ResponseEntity.ok().build();

        return ResponseEntity.notFound().build();
    }

    @GetMapping(path = "/{id}/formaciones", produces = "application/json")
    public ResponseEntity<List<Formacion>> getFormaEmpleado(@PathVariable int id) {
        List<Formacion> forms = empleadosDAO.getFormaEmpleado(id);
        if (forms != null) return ResponseEntity.ok().body(forms);

        return ResponseEntity.notFound().build();
    }

    @PostMapping(path = "/{id}/formaciones", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addFormaEmpleado(@PathVariable int id, @RequestBody Formacion forma) {
        Empleado emp = empleadosDAO.addFormaEmpleado(id, forma);
        if (emp != null) return ResponseEntity.ok().build();

        return ResponseEntity.notFound().build();
    }

    @Value("${app.titulo}") private String titulo;
    @GetMapping("/titulo")
    public String getTitulo(){
        String str = config.getTitulo() + " " + config.getModo();
        return str;
    }

    @Autowired private Environment environment;
    @GetMapping("/prop")
    public String getProp(@RequestParam("key") String key) {
        String valor = "Sin valor";
        String keyVal = environment.getProperty(key);

        if (keyVal != null && !keyVal.isEmpty()) {
            valor = keyVal;
        }
        return valor;
    }

    @Autowired private Configuracion config;
    @GetMapping("/autor")
    public String getAutor() {
        return config.getAutor();
    }

    @GetMapping("/home")
    public String home(){
        return "¡Bienvenido!";
    }

    @GetMapping("/empleados/cadena")
    public String getCadena(@RequestParam String texto, @RequestParam String separador){
        try {
            return Utilidades.getCadena(texto, separador);
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }
}
