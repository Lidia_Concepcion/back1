package com.techuniversity.emp.model;

import java.util.ArrayList;

public class Empleado {

    private int id;
    private String nombre;
    private String apellidos;
    private String email;
    private ArrayList<Formacion> formaciones;

    public Empleado(){ }

    public Empleado(int id, String nombre, String apellidos, String email, ArrayList<Formacion> formaciones) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.formaciones = formaciones;
    }

    public ArrayList<Formacion> getFormaciones() {
        return formaciones;
    }

    public void setFormaciones(ArrayList<Formacion> formaciones) {
        this.formaciones = formaciones;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Empleado{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
