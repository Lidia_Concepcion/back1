package com.techuniversity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;

public class Users {
    private static JSONObject data = null;

    public static void cargarUsers() throws FileNotFoundException{
        try(InputStream resourceAsStream = Users.class.getClassLoader().getResourceAsStream("clientes.json")){
            InputStreamReader isr = new InputStreamReader(resourceAsStream);
            BufferedReader br = new BufferedReader(isr);
            String line;
            String str = "";
            while ((line = br.readLine()) != null){
                str += line;
            }
            data = new JSONObject(str);

        } catch (IOException fe){
            System.out.println(fe.getMessage());
        } catch (JSONException je){
            System.out.println(je.getMessage());
        }
    }

    public static JSONObject getUser(int id) throws Exception {
        if (data == null) cargarUsers();
        JSONObject selUser = null;
        JSONArray users = data.getJSONArray("users");
        for (int i=0; i < users.length(); i++){
            JSONObject us = users.getJSONObject(i);
            int idUser = us.getInt("userId");
            if (idUser == id){
                selUser = us;
                break;
            }
        }
        return selUser;
    }
}
