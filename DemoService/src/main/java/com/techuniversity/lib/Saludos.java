package com.techuniversity.lib;

public class Saludos {
    public String getSaludoCordial(String nombre){
        return String.format("Bienvenido %s", nombre);
    }

    public String getSaludoInformal(String nombre){

        return String.format("Ey %s. ¿Qué pasa, tío?", nombre);
    }
}
