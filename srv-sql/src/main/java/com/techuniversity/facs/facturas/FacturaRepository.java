package com.techuniversity.facs.facturas;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FacturaRepository extends CrudRepository<FacturaModel, Integer> {
    List<FacturaModel> findByImporte(double desde);
    List<FacturaModel> findByFecha(String fecha);
}
